#!/bin/bash

rm -rf /var/www/html/*;
python venus/planet.py planetconfig.ini;
python venus/planet.py design.ini;
cp -r /vagrant/sub-planets/people/images-v2/ /var/www/html/;
cp -r /vagrant/sub-planets/people/css-v2/ /var/www/html/;
