# fedoraplanet-theme

This repo contains the theme for the Fedora Planet.

## Hacking

Hacking on the theme is done using Vagrant. The vagrant setup in this repo
creates a basic environment to test and hack on the theme.

First, install Vagrant, vagrant-libvirt, and vagrant-sshfs from the official
Fedora repos:

    $ sudo dnf install vagrant vagrant-libvirt vagrant-sshfs


Now, fork this repo here on Pagure, and clone it to your machine. Then, from
within main directory (the one with the Vagrantfile in it) of your git checkout,
run the ``vagrant up`` command to provision your dev environment:

    $ vagrant up


When this command is completed (it may take a while), simply go to
http://localhost:8080/ on in the browser on your host to see your sample planet,
 ready to hack on.

 When you have made a change to your theme in the checkout of the theme, run the
 following command to regenerate the sample planet with your changes:

    $ vagrant ssh -c "pushd /home/vagrant/; sudo ./generate.sh"
